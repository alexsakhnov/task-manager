<?php

use SimpleTask\Model\Repository;
use SimpleTask\Model\Task;
use SimpleTask\View\Render;

require "../vendor/autoload.php";

/* Считываем конфигурацию и создаём сервисы. */
$configFile = '../settings.ini';
$config = parse_ini_file($configFile, true);
if ($config === false)
{
    echo "Failed to read config file: $configFile";
    exit();
}
$repo = new Repository(
    $config['db']['dsn'], $config['db']['user'], $config['db']['pass']
);
$render = new Render();

$routes = [];

/* Добавление задачи. */
$routes['add'] = function () use ($repo, $render) {
    $task = new Task();
    $added = false;
    $errors = [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $task->email = trim($_POST['email']);
        $task->user = trim($_POST['user']);
        $task->text = trim($_POST['text']);

        if ($task->validate($errors)) {
            $repo->save($task);
            $added = true;
            $task = new Task();
        }
    }

    $render->render(
        'add',
        [
            'task' => $task,
            'added' => $added,
            'errors' => $errors,
        ]
    );
};

/* Редактирование задачи. */
$routes['edit'] = function () use ($repo, $render) {
    $id = (int)($_GET['id'] ?? 0);
    $task = $repo->find($id);
    $updated = false;
    $errors = [];
    $isAdmin = isset($_SESSION['user']) && $_SESSION['user'] === 'admin';

    if (!isset($task)) {
        header("HTTP/1.0 404 Not Found");
    } elseif (!$isAdmin) {
        header("HTTP/1.1 401 Unauthorized");
    } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $text = trim($_POST['text']);
        $task->edited = $task->edited || ($text != $task->text);
        $task->text = $text;
        $task->status = isset($_POST['closed']) ? Task::STATUS_CLOSED : Task::STATUS_OPEN;

        if ($task->validate($errors)) {
            $repo->save($task);
            $updated = true;
        }
    }

    $render->render(
        'edit',
        [
            'task' => $task,
            'updated' => $updated,
            'errors' => $errors,
            'isAdmin' => $isAdmin,
        ]
    );
};

/* Вход на сайт. */
$routes['login'] = function () use ($render, $config) {
    $error = null;
    $loggedIn = isset($_SESSION['user']);

    if (!$loggedIn && $_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['user'] === $config['auth']['user'] &&
            $_POST['password'] === $config['auth']['pass']) {
            $_SESSION['user'] = $_POST['user'];
            header("Location: /");
            return;
        } else {
            $error = "Имя пользователя или пароль указаны некорректно";
        }
    }

    $render->render(
        'login',
        ['error' => $error, 'loggedIn' => $loggedIn]
    );
};

/* Выход пользователя. */
$routes['logout'] = function () {
    unset($_SESSION['user']);
    header("Location: /");
};

/* Список задач. */
$routes['list'] = function () use ($repo, $render) {
    $limit = 3;
    $asc = !isset($_GET['desc']);
    $page = max(1, (int)$_GET['page']);
    $order = (string)$_GET['order'];
    if (!in_array($order, ["id", "user", "email", "text", "status"])) {
        $order = "id";
    }

    $count = $repo->count();
    $lastPage = ceil($count / $limit);

    $url = function (?int $urlPage, ?string $urlOrder = null, ?bool $urlAsc = null) use ($order, $page, $asc) {
        $urlOrder = $urlOrder ?? $order;
        $urlPage = $urlPage ?? $page;
        $urlAsc = $urlAsc ?? $asc;

        return htmlspecialchars("?page=$urlPage&order=$urlOrder" . ($urlAsc ? "" : "&desc"));
    };

    $render->render(
        'list',
        [
            'isAdmin' => isset($_SESSION['user']) && $_SESSION['user'] == 'admin',
            'tasks' => $repo->load($order, $limit, ($page - 1) * $limit, $asc),
            'url' => $url,
            'count' => $count,
            'page' => $page,
            'asc' => $asc,
            'order' => $order,
            'lastPage' => $lastPage,
        ]
    );
};

/* Определяем запрошенную страницы и выводим её. */
session_start();
$action = $_GET['action'] ?? 'list';
if (isset($routes[$action])) {
    $routes[$action]();
} else {
    header("HTTP/1.0 404 Not Found");
    echo "404 Not found";
}
