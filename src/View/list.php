<?
/**
 * @var \SimpleTask\Model\Task[] $tasks
 * @var string $order Порядок сортировки
 * @var bool $asc Сортировка по возрастанию
 * @var int $page Номер текущей страницы
 * @var int $lastPage Номер последней страницы
 * @var callable $url Функция для генерации URL: url($page, $order, $asc)
 * @var bool $isAdmin Пользователь администратор
 */

?>

<div class="table-responsive">
<table class="table">
    <thead>
    <tr>
        <?
        $columnHeader = function ($title, $field) use ($url, $order, $asc) {
            echo '<a href="' . $url(null, $field, $order != $field ?: !$asc) . '">' .
                $title .
                (($order == $field) ? ($asc ? " ↑" : " ↓") : "").
                '</a>';
        } ?>

        <th scope="col"><?= $columnHeader("Имя пользователя", "user") ?></th>
        <th scope="col"><?= $columnHeader("E-mail", "email") ?></th>
        <th scope="col">Текст задачи</th>
        <th scope="col"><?= $columnHeader("Статус", "status") ?></th>
        <? if ($isAdmin): ?>
        <th scope="col"></th>
        <? endif; ?>
    </tr>
    </thead>
    <tbody>

    <?
    foreach ($tasks as $task): ?>
        <tr>
            <td><?= htmlspecialchars($task->user) ?></td>
            <td><?= htmlspecialchars($task->email) ?></td>
            <td>
                <?= nl2br(htmlspecialchars($task->text)) ?>
                <? if ($task->edited): ?>
                    <br/>
                    <small class="text-muted">Отредактировано администратором</small>
                <? endif; ?>
            </td>
            <td><?= htmlspecialchars($task->statusName()) ?></td>
            <? if ($isAdmin): ?>
            <td>
                <a href="/?action=edit&id=<?=$task->id ?>" title="Редактировать задачу">Ред.</a>
            </td>
            <? endif; ?>
        </tr>
    <?
    endforeach; ?>

    </tbody>
</table>
</div>

<nav aria-label="Навигация по списку">
    <ul class="pagination justify-content-center">
        <?
        if ($page > 1): ?>
            <li class="page-item">
                <a class="page-link" href="<?= $url($page - 1) ?>" tabindex="-1" aria-disabled="true">Назад</a>
            </li>
        <?
        endif; ?>

        <?
        for ($p = 1; $p <= $lastPage; $p++): ?>
            <li class="page-item <?= $page == $p ? "active" : "" ?>">
                <a class="page-link" href="<?= $url($p) ?>"><?= $p ?></a>
            </li>
        <?
        endfor; ?>

        <?
        if ($page < $lastPage): ?>
            <li class="page-item">
                <a class="page-link" href="<?= $url($page + 1) ?>">Вперёд</a>
            </li>
        <?
        endif; ?>

    </ul>
</nav>
