<?
/**
 * @var Task $task
 * @var bool $updated
 * @var bool $isAdmin
 * @var array $errors
 */

use SimpleTask\Model\Task;

?>

<?
function renderError($errors, $name): string
{
    if (isset($errors[$name])) {
        return '<div class="invalid-feedback">' . htmlspecialchars($errors[$name]) . '</div>';
    }

    return '';
}

?>

<?
if ($updated): ?>
    <div class="alert alert-success" role="alert">
        Задача успешно обновлена!
    </div>
<?
endif; ?>

<?
if (count($errors) > 0): ?>
    <div class="alert alert-danger" role="alert">
        Ошибка! Некоторые поля заполнены некорректно.
    </div>
<?
endif; ?>

<?
if ($isAdmin): ?>
    <form action="/?action=edit&id=<?= $task->id ?>" method="post">
        <div class="form-group">
            <label for="taskText">Текст задачи</label>
            <textarea class="form-control <?= isset($errors['text']) ? "is-invalid" : "" ?>"
                      id="taskText" name="text"
                      rows="3"><?= htmlspecialchars($task->text); ?></textarea>

            <?= renderError($errors, 'text'); ?>
        </div>

        <div class="form-group form-check">
            <input class="form-check-input"
                <?= $task->status == Task::STATUS_CLOSED ? "checked" : "" ?>
                   type="checkbox" name="closed" value="" id="taskClosed">
            <label class="form-check-label" for="taskClosed">
                Выполнено
            </label>
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
<?
else: ?>
    <div class="alert alert-danger" role="alert">
        Для редактирования задачи необходимо авторизоваться.
    </div>
<?
endif; ?>
