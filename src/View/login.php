<?
/**
 * @var \SimpleTask\Model\Task $task
 * @var bool $loggedIn
 * @var string $error
 */

?>

<?
if (isset($error)): ?>
    <div class="alert alert-danger" role="alert"><?= htmlspecialchars($error); ?></div>
<?
endif; ?>

<?
if ($loggedIn): ?>
    <div class="alert alert-primary" role="alert">
        Вы уже вошли на сайт.
    </div>
<?
else: ?>
    <form action="/?action=login" method="post">
        <div class="form-group">
            <label for="loginUser">Имя пользователя</label>
            <input type="text"
                   class="form-control" id="loginUser" name="user">
        </div>

        <div class="form-group">
            <label for="loginPassword">Пароль</label>
            <input type="password"
                   class="form-control" id="loginPassword" name="password">
        </div>

        <button type="submit" class="btn btn-primary">Войти</button>
    </form>
<?
endif; ?>
