<?php


namespace SimpleTask\View;

/**
 * Class Render
 * @package SimpleTask\View
 *
 * Рендерер PHP-шаблонов.
 */
class Render
{

    private function renderContent($template, $variables = []): string
    {
        ob_start();

        extract($variables);
        include __DIR__ . "/$template.php";
        $content = ob_get_contents();

        ob_end_clean();

        return $content;
    }

    public function render($template, $variables = []): void
    {
        $content = $this->renderContent($template, $variables);
        $loggedIn = isset($_SESSION['user']);

        include __DIR__ . "/main.php";
    }

}
