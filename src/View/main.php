<?
/**
 * @var string $content
 * @var bool $loggedIn
 */

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Задачник</title>
</head>
<body>

<div class="container">

    <nav class="navbar navbar-expand-sm navbar-light bg-light mb-4">
        <a class="navbar-brand" href="/">Задачник</a>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/?action=add">Создать задачу</a>
            </li>
            <?
            if ($loggedIn): ?>
                <li class="nav-item">
                    <a class="nav-link" href="/?action=logout">Выход</a>
                </li>
            <?
            else: ?>
                <li class="nav-item">
                    <a class="nav-link" href="/?action=login">Вход</a>
                </li>

            <?
            endif; ?>

        </ul>
    </nav>

    <div class="row">
        <div class="col">
            <?= $content; ?>
        </div>
    </div>

</div>

</body>
</html>
