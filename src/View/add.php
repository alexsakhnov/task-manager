<?
/**
 * @var \SimpleTask\Model\Task $task
 * @var bool $added
 * @var array $errors
 */

?>

<?
function renderError($errors, $name): string
{
    if (isset($errors[$name])) {
        return '<div class="invalid-feedback">' . htmlspecialchars($errors[$name]) . '</div>';
    }

    return '';
}

?>

<? if ($added): ?>
    <div class="alert alert-success" role="alert">
        Задача успешно добавлена!
    </div>
<? endif; ?>

<? if (count($errors) > 0): ?>
    <div class="alert alert-danger" role="alert">
        Ошибка! Некоторые поля заполнены некорректно.
    </div>
<? endif; ?>

<form action="/?action=add" method="post">
    <div class="form-group">
        <label for="taskEmail">E-mail</label>
        <input type="text"
               class="form-control <?= isset($errors['email']) ? "is-invalid" : "" ?>"
               id="taskEmail" placeholder="name@example.com"
               name="email" value="<?= htmlspecialchars($task->email); ?>">

        <?= renderError($errors, 'email'); ?>
    </div>

    <div class="form-group">
        <label for="taskUser">Имя пользователя</label>
        <input type="text"
               class="form-control <?= isset($errors['user']) ? "is-invalid" : "" ?>"
               id="taskUser" placeholder="Иван Иванов"
               name="user" value="<?= htmlspecialchars($task->user); ?>">

        <?= renderError($errors, 'user'); ?>
    </div>

    <div class="form-group">
        <label for="taskText">Текст задачи</label>
        <textarea class="form-control <?= isset($errors['text']) ? "is-invalid" : "" ?>"
                  id="taskText" name="text"
                  rows="3"><?= htmlspecialchars($task->text); ?></textarea>

        <?= renderError($errors, 'text'); ?>
    </div>

    <button type="submit" class="btn btn-primary">Создать задачу</button>
</form>
