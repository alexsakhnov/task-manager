<?php

namespace SimpleTask\Model;

/**
 * Class Task
 *
 * Задача
 */
class Task
{
    public const STATUS_OPEN = 0;
    public const STATUS_CLOSED = 1;

    /* Идентификатор. */
    public $id;

    /**
     * Имя пользователя
     * @var string
     */
    public $user;

    /**
     * E-mail
     * @var string
     */
    public $email;

    /**
     * Текст
     * @var string
     */
    public $text;

    /**
     * Статус
     * @var int
     */
    public $status = self::STATUS_OPEN;

    /**
     * Отредактировано администратором
     * @var bool
     */
    public $edited = false;

    public function statusName(): string
    {
        switch ($this->status) {
            case self::STATUS_OPEN:
                return 'Открыто';
            case self::STATUS_CLOSED:
                return 'Выполнено';
            default:
                return '???';
        }
    }

    public function validate(&$errors = []): bool
    {
        if (empty($this->email)) {
            $errors['email'] = 'Укажите e-mail';
        } elseif (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Некорректный e-mail';
        } elseif (strlen($this->email) > 255) {
            $errors['email'] = 'Максимальная длина e-mail - 255 символов';
        }

        if (empty($this->user)) {
            $errors['user'] = 'Укажите имя пользователя';
        } elseif (strlen($this->user) > 255) {
            $errors['user'] = 'Максимальная длина имени пользователя - 255 символов';
        }

        if (empty($this->text)) {
            $errors['text'] = 'Введите текст задачи';
        }

        if (!in_array($this->status, [self::STATUS_OPEN, self::STATUS_CLOSED], true)) {
            $errors['status'] = 'Указанный статус не существует';
        }

        return count($errors) == 0;
    }
}
