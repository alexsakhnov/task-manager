<?php


namespace SimpleTask\Model;


use PDO;

/**
 * Class Repository
 * @package SimpleTask\Model
 *
 * Репозиторий задач. Сохраняет и загружает задачи из БД.
 */
class Repository
{

    private $dsn;
    private $user;
    private $pass;
    private $connection;

    public function __construct(string $dsn, string $user, string $pass)
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->pass = $pass;
    }

    private function getConn(): PDO
    {
        if (!isset($this->connection)) {
            $this->connection = new PDO($this->dsn, $this->user, $this->pass);
        }

        return $this->connection;
    }

    private function add(Task $task): bool
    {
        $stmt = $this->getConn()->prepare(
            "INSERT INTO task (user, email, text, status, edited) VALUE (:user, :email, :text, :status, :edited)"
        );

        if (!$stmt->execute(
            [
                ':user' => $task->user,
                ':email' => $task->email,
                ':text' => $task->text,
                ':status' => $task->status,
                ':edited' => (int) $task->edited,
            ]
        )) {
            return false;
        }

        $task->id = (int)$this->getConn()->lastInsertId();
        return true;
    }

    private function update(Task $task): bool
    {
        $stmt = $this->getConn()->prepare(
            "UPDATE task SET user = :user, email = :email, text = :text, status = :status, edited = :edited WHERE id = :id"
        );

        return $stmt->execute(
            [
                ':user' => $this->user,
                ':email' => $task->email,
                ':text' => $task->text,
                ':status' => $task->status,
                ':edited' => (int) $task->edited,
                ':id' => $task->id,
            ]
        );
    }

    public function save(Task $task): bool
    {
        if ($task->id === null) {
            return $this->add($task);
        } else {
            return $this->update($task);
        }
    }

    public function load(string $orderBy = "id", int $rowCount = 3, int $offset = 1, bool $asc = false): array
    {
        $direction = $asc ? "ASC" : "DESC";

        $stmt = $this->getConn()->prepare(
            "SELECT * FROM task ORDER BY $orderBy $direction LIMIT :offset, :row_count"
        );

        $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        $stmt->bindValue(':row_count', $rowCount, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, Task::class);
    }

    public function count(): int
    {
        $stmt = $this->getConn()->prepare(
            "SELECT COUNT(*) FROM task"
        );

        $stmt->execute();

        return (int)$stmt->fetch(PDO::FETCH_COLUMN);
    }

    public function find(int $id): ?Task
    {
        $stmt = $this->getConn()->prepare(
            "SELECT * FROM task WHERE id = :id"
        );

        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchObject(Task::class);
    }
}
